// number of times each word appears in the string

function countWords(str) {
    const wordCounts = new Map()
    str.split(' ').forEach(word => {
      const currentWordCount = wordCounts.get(word) || 0
      wordCounts.set(word, currentWordCount+1)
    })
  
    /* Reproduce your output */
    const resultWords = [...wordCounts.keys()]
    const resultCount = [...wordCounts.values()]
    console.log('resultWords: ' + resultWords);
    console.log('resultCount: ' + resultCount);
  
    return wordCounts
  }

  function countWords(str) {
    const wordCounts = {}
    str.split(' ').forEach(function(word) {
      const currentWordCount = wordCounts[word] || 0
      wordCounts[word] = currentWordCount+1
    })
  
    /* Reproduce your output */
    const resultWords = Object.keys(wordCounts)
    const resultCount = resultWords.map(function(word) { return wordCounts[word] })
    console.log('resultWords: ' + resultWords);
    console.log('resultCount: ' + resultCount);
  
    return wordCounts
  }

  